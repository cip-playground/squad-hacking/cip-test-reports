""" SQUAD client fetcher module """

import ast


def shorten_link(link):
    """ Create markdown link named as string behind the last slash """
    if link.endswith('/'):
        link = link[:-1]
    last = link.split('/')[-1]
    return f'[{last}]({link})'


def fetch_project_jobs(project):
    """ Fetch all test jobs for a given project """
    for build in project.builds().values():
        for job in build.testjobs().values():
            yield build, job


def fetch_incomplete_jobs(project):
    """ Iterable for incomplete jobs including error data """

    yield ('Version', 'SQUAD Job', 'LAVA Job', 'Environment', 'Suite', 
           'Error Message', 'Error Type')

    no_entries = True
    for build, job in fetch_project_jobs(project):
        if job.job_status != 'Incomplete':
            continue
        no_entries = False
        failure = {'error_msg': 'N/A', 'error_type': 'N/A'}
        if job.failure:
            failure = ast.literal_eval(job.failure)
        suite = job.name.split('_')[-1]
        yield (
            f'`{build.version}`',
            shorten_link(job.url),
            shorten_link(job.external_url),
            f'`{job.environment}`',
            f'`{suite}`',
            failure['error_msg'],
            failure['error_type'],
        )

    if no_entries: # to assure rendering for empty table
        yield (' ', )*7
