"""
Generate html tables of incomplete jobs for a given SQUAD groups
"""

import sys
from string import Template
import pathlib
import tabulate as tb
import markdown2 as md
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad
import fetcher


def html_template(html_body_str, template_file='template.html'):
    """ Generates a full html file string for a given html body """
    with open(template_file, 'r', encoding='utf-8') as tmpl_file:
        template = Template(tmpl_file.read())
    return template.substitute(html_body=html_body_str)


def incomplete_project_jobs(squad_url, group_name, project_name, output_dir, 
                            output_file="incomplete_jobs.html"):
    """ Generate an overview of incomplete jobs for a given SQUAD project """

    SquadApi.configure(url=squad_url)
    group = Squad().group(group_name)
    project = group.project(project_name)

    gen = fetcher.fetch_incomplete_jobs(project)
    header = next(gen)
    markdown_str = f'# Incomplete jobs in `{project_name}`\n' \
                 + tb.tabulate(list(gen), header, tablefmt='github')
    html_str = html_template(md.markdown(markdown_str, extras=['tables']))

    with open(output_dir / output_file, 'w', encoding='utf-8') as html_file:
        html_file.write(html_str)


def incomplete_group_jobs(squad_url, output_dir, groups):
    """ Generate an overview of incomplete jobs for given SQUAD groups """
    SquadApi.configure(url=squad_url)
    for group in groups:
        for project in Squad().group(group).projects().values():
            incomplete_project_jobs(
                squad_url=squad_url,
                group_name=group,
                project_name=project.slug,
                output_dir=output_dir,
                output_file=f'incomplete_jobs_{group}_{project.slug}.html',
            )


if __name__ == '__main__':
    out = pathlib.Path().cwd()
    if len(sys.argv) > 1:
        out = pathlib.Path(sys.argv[1])
    incomplete_group_jobs(
        squad_url='https://squad.ciplatform.org/',
        output_dir=out,
        groups=('linux-cip', 'linux-stable-rc'),
    )
