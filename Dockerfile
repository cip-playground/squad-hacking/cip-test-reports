FROM python:3.9-slim

RUN apt-get update
RUN apt-get install -y --no-install-recommends tree
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

RUN pip install squad-client tabulate markdown2
